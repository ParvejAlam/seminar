<!DOCTYPE html>
<html>
<head>
    <title>Registration Form</title>
</head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/fonts/fontawesome/font-awesome.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/preset.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('public/css/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/jquery-ui.css')}}">
<body style="background-color: #9c9c9c14;">
@if(Session::has('errMsg'))
<div class="alert alert-info fade in alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>{{ Session::get('errMsg') }}</strong>
</div>
@endif

    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                <div id="box">

    
<img src="{{asset('public/img/logo.png')}}" class="pull-left logo">      
<img src="{{asset('public/img/eze.png')}}" class="pull-right logo">     
<h1>Registration Form</h1> 
<hr class="m-t-10 m-b-0">
<div class="heading m-t-10 m-b-0">
    Personal Details
</div>
<form method="post" action="{{route('FormExec')}}">
{{ csrf_field() }}
<div class="input-group">
    <label class="input-group-addon">Full Name :</label>
    <input class="form-control" type="text" name="FullName" required="">
</div>
<div class="input-group">
    <label class="input-group-addon">Contact No :</label>
    <input class="form-control" type="text" name="ContactNo" required="">
</div>
<div class="input-group">
    <label class="input-group-addon">Email Id :</label>
    <input class="form-control" type="text" name="EmailId" required="">
</div>
<div class="input-group">
    <label class="input-group-addon">Address :</label>
    <input class="form-control" type="text" name="Address" required="">
</div>
<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="input-group">
            <label class="input-group-addon">Gender :</label>
            <div class="radioCon">
                <input type="radio" name="Gender" value="M" required=""> Male &nbsp;
                <input type="radio" name="Gender" value="F" required=""> Female
            </div>
        </div>
    </div>
    <div class="col-sm-8 col-xs-12">
        <div class="input-group">
            <label class="input-group-addon">D.O.B :</label>
            <input type="date" name="DOB" class="form-control" required="">
        </div>
    </div>
</div>

<div class="heading m-t-20 m-b-0">
    Education Details
</div>

<div class="input-group">
    <label class="input-group-addon">Current Semester :</label>
    <input class="form-control" type="text" name="CurrentSemester">
</div>
<div class="input-group">
    <label class="input-group-addon">Current Branch :</label>
    <input class="form-control" type="text" name="CurrentBranch">
</div>
<table class="table table-bordered m-t-20">
    <thead>
        <tr>
            <th></th>
            <th>Year</th>
            <th>Institute/Board</th>
            <th>Branch/Stream</th>
            <th>Percentage/Grade</th>
        </tr>
        <tbody>
            <tr>
                <td>
                    X<sup>th</sup>
                </td>
                <td>
                    <input class="form-control" type="text" name="XYear">
                </td>
                <td>
                    <input class="form-control" type="text" name="XInstitute">
                </td>
                <td>
                    <input class="form-control" type="text" name="XBranch">
                </td>
                <td>
                    <input class="form-control" type="text" name="XPercentage">
                </td>
            </tr>
            <tr>
                <td>
                    XII<sup>th</sup>
                </td>
                <td>
                    <input class="form-control" type="text" name="XIIYear">
                </td>
                <td>
                    <input class="form-control" type="text" name="XIIInstitute">
                </td>
                <td>
                    <input class="form-control" type="text" name="XIIBranch">
                </td>
                <td>
                    <input class="form-control" type="text" name="XIIPercentage">
                </td>
            </tr>
            <tr>
                <td>
                    Graduation
                </td>
                <td>
                    <input class="form-control" type="text" name="GYear">
                </td>
                <td>
                    <input class="form-control" type="text" name="GInstitute">
                </td>
                <td>
                    <input class="form-control" type="text" name="GBranch">
                </td>
                <td>
                    <input class="form-control" type="text" name="GPercentage">
                </td>
            </tr>
            <tr>
                <td>
                    Post Graduation
                </td>
                <td>
                    <input class="form-control" type="text" name="PGYear">
                </td>
                <td>
                    <input class="form-control" type="text" name="PGInstitute">
                </td>
                <td>
                    <input class="form-control" type="text" name="PGBranch">
                </td>
                <td>
                    <input class="form-control" type="text" name="PGPercentage">
                </td>
            </tr>
        </tbody>
    </thead>
</table>

<div class="heading m-t-20 m-b-0">
    Courses Offered by NANSAT with EZE.AI
</div>
<h3>Interested Areas(Choose all that apply)</h3>

<div class="m-t-5">
    <input type="checkbox" name="InterestedArea[]" value="AI"> <strong>Algorithms & A.I</strong> (Machine Learning with project)
</div>
<div class="m-t-5">
    <input type="checkbox" name="InterestedArea[]" value="MEAN"> <strong>MEAN Stack Development</strong> (Node.js, React.js & Cloud Services)
</div>
<div class="m-t-5">
    <input type="checkbox" name="InterestedArea[]" value="BlockChain"> <strong>BlockChain Development</strong> (Solidity and Hyperledger)
</div>


<div class="m-t-70">
    <div class="input-group">
        <label class="input-group-addon">What are you looking for :</label>
        <input class="form-control" type="text" name="CurrentSemester">
    </div>
</div>

<button type="submit" class="btn customButton m-t-20"><i class="fa fa-save"></i> SAVE</button>
</form>
<hr class="m-t-30 m-b-0">
<footer>
    <h4>NANSAT SOFTWARE DEVELOPMENT & TRAINING</h4>
    <div class="row">
        <div class="col-sm-8 col-xs-12 text-left">
            <div class="address">WWW.NANSAT.COM</div>
            <div class="address">ADD: 2<sup>nd</sup> FLOOR, 178 SEC-10, ON SEC 9/10 DIVIDING ROAD, FARIDABAD.</div>
        </div>
        <div class="col-sm-4 col-xs-12 text-right">
            <div class="address">WWW.EZE.AI</div>
            <div class="address">PH: +919871775116, +919811821102</div>
        </div>
    </div>
</footer>





                </div>
            </div>
        </div>
    </div>





    <script src="{{asset('public/js/jquery-3.2.1.min.js')}}"></script>  
    <script src="{{asset('public/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/js/jquery-ui.js')}}"></script>
</body>
</html>