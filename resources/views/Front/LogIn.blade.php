
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Kodinger">
    <title>My Login Page &mdash; Bootstrap 4 Login Page Snippet</title>
    <link rel="stylesheet" type="text/css" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/css/my-login.css')}}">
</head>
<body class="my-login-page">
    <section class="h-100">
        <div class="container h-100">
            <div class="row justify-content-md-center h-100">
                <div class="card-wrapper">
                    <div class="brand">
                        <img src="{{asset('public/img/logo.jpg')}}">
                    </div>
                    <div class="card fat">
                        <div class="card-body">
                            <h4 class="card-title">Login</h4>
                            {{Form::open(["url"=>route("LogInExec")])}}
                                <div class="form-group">
                                    <label for="email">E-Mail Address</label>
                                    <input id="email" type="text" class="form-control" name="email" value="" autofocus>
                                    {!!$errors->first("email","<span class='error'>:message</span>")!!}
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control" name="password" data-eye>
                                    {!!$errors->first("password","<span class='error'>:message</span>")!!}
                                </div>


                                <div class="form-group no-margin">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Login
                                    </button>
                                </div>
@if(Session::has('errMsg'))
<span class='error'>{{ Session::get('errMsg') }}</span>
@endif
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{asset('public/js/jquery.min.js')}}"></script>
    <script src="{{asset('public/bootstrap/js/popper.min.js')}}"></script>
    <script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>