<!DOCTYPE html>
	<html>
<head>
<title>CRM</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/bootstrap/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('public/fonts/fontawesome/font-awesome.min.css')}}">
<!--===============================================================================================-->
<link type="text/css" rel="stylesheet" href="{{asset('public/css/preset.css')}}"/>
<!--===============================================================================================-->
<link type="text/css" rel="stylesheet" href="{{asset('public/css/style.css')}}"/>
  <link rel="stylesheet" href="{{asset('public/css/jquery-ui.css')}}">
<!--===============================================================================================-->
<style type="text/css">
  .custom-success-alert{
        z-index: 1;
    position: fixed;
    bottom: 0px;
    right: 0px;
  }
</style>
@yield('internalCss')
</head>
<body>
<!-- header start -->

@if (Session::has('msg'))
  <div class="alert alert-success custom-success-alert alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {!! Session::get('msg') !!}
  </div>
@endif
@if (Session::has('errMsg'))
  <div class="alert alert-danger custom-success-alert alert-dismissible fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> {!! Session::get('errMsg') !!}
  </div>
@endif
  {!! $errors->first('msg','
      <div class="alert custom-success-alert alert-success alert-dismissable fade in">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
          <strong>Success!</strong> :message
      </div> 
  ') !!}

  {!! $errors->first('errMsg','
      <div class="alert custom-danger-alert alert-danger alert-dismissable fade in">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Error!</strong> :message
      </div>
  ') !!}

<header style="">
  <div class="row">
    <div class="col-xs-12 header">
      <h1> <span class="fa fa-bars"></span>
         <a href="{{route('LogOut')}}"><i class="fa fa-sign-out"></i> logout</a>
         <!-- <a href=""><i class="fa fa-user"></i> profile</a> -->
	  </h1>
    </div>
</div>
</header>
<!-- header ended -->
<!-- sidebar start -->
<aside>
<div class="heading1 col-xs-12">
<img src="{{asset('public/img/logo.png')}}" alt="logo" />
</div>
<div class="nav slideleft col-xs-12">
<ul>
   <li class="active link"><a href="{{route('Home')}}" ><i class="fa fa-dashboard"></i> Dashboard</a></li>

    <li class="link">
      <a href="javascript:void(0)">
        <i class="fa fa-cog"></i>
        Master
        <span class="fa fa-angle-down"></span>
      </a>
      <ul>
        <li class="link"><a href=""> <i class="fa fa-circle-o"></i>Courses</a></li>
     </ul>
   </li>

   
</ul>
</div>
</aside>


<div class="col-xs-12 content-section">
			  <!-- content section -->
               @yield("content")
			  <!-- content section -->
<!-- table section -->
</div>
</div>
<script src="{{asset('public/js/jquery-3.2.1.min.js')}}"></script>  
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/jquery-ui.js')}}"></script>
<script src="{{asset('public/js/script.js')}}"></script>

@yield("internalJs")
</body>
</html>
