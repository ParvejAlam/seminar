<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["middleware"=>"checkNotLoggedIn"],function(){
	Route::get("/","FrontController@Form")->name("Form");
	Route::post("FormExec","FrontController@FormExec")->name("FormExec");
	Route::group(["prefix"=>"Front"],function(){
		Route::get('LogIn', "FrontController@LogIn")->name("LogIn");
		Route::post('LogInExec','FrontController@LogInExec')->name("LogInExec");
	});
});
Route::group(["middleware"=>"checkLoggedIn"],function(){
	Route::group(["prefix"=>"Dashboard"],function(){
		Route::get("Home","DashboardController@Home")->name("Home");
		Route::get("LogOut",function(){Session::flush(); return redirect()->route('LogIn');})->name("LogOut");
	});
});
