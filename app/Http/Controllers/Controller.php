<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public static function createUserSession ($clientDetails = array()) {
        if (isset($clientDetails) && !empty($clientDetails)) {
            if (!Session::has('user_session')) {
                Session::push('user_session', $clientDetails);
                Session::save();
                // dd(Session::has('user_session'));
            }
        }
        else {
            return false;
        }
            return true;
             //   dd(Session::has('user_session'));

    }
}
