<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
class FrontController extends Controller
{
    public function LogIn(){
    	return view("Front.LogIn");
    }
    public function LogInExec(Request $request){
    	$this->validate($request,[
    		"email"=>"required",
    		"password"=>"required"
    	]);
    	$result=DB::table('users')->where('email',$request->email)->where('isActive',1)->first();
        if($result!=NULL){
            $pass=(isset($result->password) && !empty($result->password)) ? $result->password:"";
            if(Hash::check($request['password'],$pass)){
                $activeUserDetails=DB::table('users')
                ->where('users.userId',$result->userId)
                ->leftJoin('user_role_xref', 'users.userId', '=', 'user_role_xref.userId')
                ->leftJoin('roles', 'roles.roleId', '=', 'user_role_xref.roleId')
                ->select('users.*','roles.*')
                ->first();
                if(Self::createUserSession($activeUserDetails)){
                    return redirect()->route('Home');
                }
            }else{
                $request->session()->flash('errMsg', 'email and password do not match');
                return redirect()->back()->withInput();
            }
        }else{
                $request->session()->flash('errMsg', 'invalid email');
                return redirect()->back()->withInput();
        }

    }
    public function Form(){
        return view("Front.Form");
    }
    public function FormExec(Request $request){
        $insert=[
            "FullName"=>$request->FullName,
            "ContactNo"=>$request->ContactNo,
            "EmailId"=>$request->EmailId,
            "Address"=>$request->Address,
            "Gender"=>$request->Gender,
            "DOB"=>$request->DOB,
            "CurrentSemester"=>$request->CurrentSemester,
            "CurrentBranch"=>$request->CurrentBranch,
            "XYear"=>$request->XYear,
            "XInstitute"=>$request->XInstitute,
            "XBranch"=>$request->XBranch,
            "XPercentage"=>$request->XPercentage,
            "XIIYear"=>$request->XIIYear,
            "XIIInstitute"=>$request->XIIInstitute,
            "XIIBranch"=>$request->XIIBranch,
            "XIIPercentage"=>$request->XIIPercentage,
            "GYear"=>$request->GYear,
            "GInstitute"=>$request->GInstitute,
            "GBranch"=>$request->GBranch,
            "GPercentage"=>$request->GPercentage,
            "PGYear"=>$request->PGYear,
            "PGInstitute"=>$request->PGInstitute,
            "PGBranch"=>$request->PGBranch,
            "PGPercentage"=>$request->PGPercentage,
            "InterestedArea"=>($request->InterestedArea)?implode(",",$request->InterestedArea):"",
        ];
        if(DB::table("registration")->insert($insert)){
            $request->session()->flash('errMsg', 'Thank you for giving me your precious time');
            return redirect()->route("Form");
        }
        else{
            $request->session()->flash('errMsg', 'Database Error');
            return redirect()->route("Form");

        }
    }
}
